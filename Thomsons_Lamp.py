import time
import math
x = False
timeMinutes = float(input("=========================\nWELCOME TO THOMSON'S LAMP\n=========================\nPlease enter a time in minutes: "))
timeSeconds = timeMinutes*60
startTime = time.time()
values = [0,startTime]
endTime = startTime + timeSeconds
nextToggleTime = startTime + (timeSeconds / 2)
while time.time() <= endTime:
    if time.time() >= nextToggleTime:
        values[0] = math.fabs(nextToggleTime)
        interval = math.fabs(values[0] - values[1])
        values[1] = values[0]
        x = not x
        print(x, "||", time.time() - startTime, "||", interval)
        interval /= 2
        nextToggleTime = time.time() + ((endTime - time.time()) / 2)
